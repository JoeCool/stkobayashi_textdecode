Super simple tool to decode the Text files (st.1 to st.4) of the DOS game
"Star Trek: The Kobayashi Alternative"

works for the 1987 updated version

usage: `python textdecode.py st.1`

to save it to a new file use: `python textdecode.py st.1 > newfile.txt`
