import sys, io

def swapNibbles(x):
    #x = ord(x)
    return ( (x & 0x0F)<<4 | (x & 0xF0)>>4 )

with io.FileIO(sys.argv[1]) as f:
    txt = f.read()
    txt = bytearray(txt)
    for idx,b in enumerate(txt):
        txt[idx] = swapNibbles(b)
    print txt
